/****************************************************************************
* FileName:		Demo.c
* Dependencies: none   
* Processor:	PIC18F46J50	
* Complier:     Microchip C18 v3.04 or higher
* Company:		Microchip Technology, Inc.
*
* Copyright and Disclaimer Notice for P2P Software:
*
* Copyright � 2007-2010 Microchip Technology Inc.  All rights reserved.
*
* Microchip licenses to you the right to use, modify, copy and distribute 
* Software only when embedded on a Microchip microcontroller or digital 
* signal controller and used with a Microchip radio frequency transceiver, 
* which are integrated into your product or third party product (pursuant 
* to the terms in the accompanying license agreement).   
*
* You should refer to the license agreement accompanying this Software for 
* additional information regarding your rights and obligations.
*
* SOFTWARE AND DOCUMENTATION ARE PROVIDED �AS IS� WITHOUT WARRANTY OF ANY 
* KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY 
* WARRANTY OF MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A 
* PARTICULAR PURPOSE. IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE 
* LIABLE OR OBLIGATED UNDER CONTRACT, NEGLIGENCE, STRICT LIABILITY, 
* CONTRIBUTION, BREACH OF WARRANTY, OR OTHER LEGAL EQUITABLE THEORY ANY 
* DIRECT OR INDIRECT DAMAGES OR EXPENSES INCLUDING BUT NOT LIMITED TO 
* ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES, 
* LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF SUBSTITUTE GOODS, 
* TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT 
* NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*
****************************************************************************
* File Description:
*
* Change History:
*  Rev   Date         Author    Description
*  1.0   2/02/2011    ccs       Initial revision
*  1.1   7/08/2011    mynenis   Added multi channel support
*  1.2   7/12/2011    mynenis   Range Demo Retry Fix
*  1.3   2/28/2013    mynenis   Fixed the addressing issue in non IEEE 802.15.4 and reduced the
*                               latecy caused by LCD Display
********************************************************************/

/************************ HEADERS ****************************************/
#include "ConfigApp.h"
#include "HardwareProfile.h"
#include "WirelessProtocols/MSPI.h"
#include "WirelessProtocols/MCHP_API.h"
#include "WirelessProtocols/SymbolTime.h"
#include "TimeDelay.h"
#include "MAC_EEProm.h"



// Demo Version
#define MAJOR_REV       1
#define MINOR_REV       3

/*************************************************************************/
// The variable myChannel defines the channel that the device
// is operate on. This variable will be only effective if energy scan
// (ENABLE_ED_SCAN) is not turned on. Once the energy scan is turned
// on, the operating channel will be one of the channels available with
// least amount of energy (or noise).
/*************************************************************************/

// Possible channel numbers are from 0 to 31
BYTE myChannel = 26;

#define MiWi_CHANNEL        0x04000000                          //Channel 26 bitmap

#define EXIT_DEMO           1
#define RANGE_DEMO          2
#define TEMP_DEMO           3
#define IDENTIFY_MODE       4
#define EXIT_IDENTIFY_MODE  5

#define NODE_INFO_INTERVAL  5

#define MODE_NS             0  
#define MODE_S              1

BYTE ConnectionEntry = 0;
			
BOOL NetFreezerEnable = FALSE;
BOOL NetSecure = MODE_NS;
extern BYTE myLongAddress[];

/*************************************************************************/
// AdditionalNodeID variable array defines the additional 
// information to identify a device on a PAN. This array
// will be transmitted when initiate the connection between 
// the two devices. This  variable array will be stored in 
// the Connection Entry structure of the partner device. The 
// size of this array is ADDITIONAL_NODE_ID_SIZE, defined in 
// ConfigApp.h.
// In this demo, this variable array is set to be empty.
/*************************************************************************/
#if ADDITIONAL_NODE_ID_SIZE > 0
    BYTE AdditionalNodeID[ADDITIONAL_NODE_ID_SIZE] = {0x00};
#endif
    
#define PANCO_0 0x00
#define PANCO_1 0x00
#define SHORTADDRESS_LENGTH  2  


/*********************************************************************
* Function:         void main(void)
*
* PreCondition:     none
*
* Input:		    none
*
* Output:		    none
*
* Side Effects:	    none
*
* Overview:		    This is the main function that runs the simple 
*                   
*
* Note:			    
**********************************************************************/
void main(void)
{
    BOOL pButtonPressed=FALSE;
    BYTE counter=0;
    
    //InitApp();
    BoardInit();
    InitSymbolTimer();
    modbus_init();
    
    /*******************************************************************/
    //  Almacena la direccion EUI en  myLongAddress
    /*******************************************************************/
    
 	Read_MAC_Address();//MODIFICAR PARA QUE SE ESCRIBE  DESDE CONFAPP
   
    currentChannel = myChannel;
    
    MiApp_ProtocolInit(FALSE);//recordar almacenar durante el procucto final
    MiApp_SetChannel(myChannel);
    MiApp_ConnectionMode(ENABLE_ALL_CONN);
    /*******************************************************************/
    // Create a New Network
    /*******************************************************************/
    MiApp_StartConnection(START_CONN_DIRECT,0,0);
    /*******************************************************************/
    // Wait for a Node to Join Network then proceed to Demo's
    /*******************************************************************/               
    /*
        while(!ConnectionTable[0].status.bits.isValid)
        {
            if(MiApp_MessageAvailable())
                MiApp_DiscardMessage();
        }    
	*/			 
    
    /* Configure the oscillator for the device */
    //ConfigureOscillator();

    /* Initialize I/O and Peripherals for application */
    // initialize the device
    //SYSTEM_Initialize();
    // When using interrupts, you need to set the Global and Peripheral Interrupt Enable bits
    // Use the following macros to:

    // Enable the Global Interrupts
    //INTERRUPT_GlobalInterruptEnable();

    // Enable the Peripheral Interrupts
    //INTERRUPT_PeripheralInterruptEnable();

    // Disable the Global Interrupts
    //INTERRUPT_GlobalInterruptDisable();

    // Disable the Peripheral Interrupts
    //INTERRUPT_PeripheralInterruptDisable();

    /* TODO <INSERT USER APPLICATION CODE HERE> */

    while(1)
    {   
    /******************************************************************************/
    /*       */
    /******************************************************************************/
        
    /******************************************************************************/
    /* Llama a administrar la apliaciones                                         */
    /******************************************************************************/        
        Serv_APP();        
    /******************************************************************************/
    /* Llama a administrar la conexion MODBUS en busca de nuevos paquetes arma el
     *  buffer                                                                    */
    /******************************************************************************/ 
        if (Sem_ModBus_Free) ModBus_Manage();
    }

}

    

