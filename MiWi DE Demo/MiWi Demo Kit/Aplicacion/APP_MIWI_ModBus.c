/*
 * File:   ModBus_APP.c
 * Author: cba
 * 
 * Procedimientos que atienten los paquetes Modbus y los enmarcan en el formato de MIWI 
 * 
 * Created on 23 de mayo de 2018, 12:34
 */


#include <xc.h>
#include "Modbus_S.h"
#include "modbus.h"
#include "APP_MIWI_ModBus.h"


Buffer_MODMIWI_t_ Rx_Buffer_MODMIWI; //paquete terminados por ModBusAPP recibidos

Buffer_MODMIWI_t_ Tx_Buffer_MODMIWI; //paquete terminados por ModBusAPP para transmitir

bool ModBus_UnicastAddress(uint8_t){
    
    return(false);
}
/*------------------------------------------------------------------------------
 * Verifica la existencia de tramas modbus recibidas, si estan completas
 * modbus_serial_new es modificada por la biblioteca modbus en la funcion modbus_timeout_now
 * que depende de TIMER2 y si cumple:
 * if((modbus_serial_state == MODBUS_GETDATA) && (modbus_serial_crc.d == 0x0000) && (!modbus_serial_new))
 * 
 -----------------------------------------------------------------------------*/
bool ModBus_MessageAvailable(void){
    
//return(modbus_serial_new);
    return(false);
}

bool Modbus_DiscardMessage(void){
    modbus_rx.len=0;
    return(true);
}

/*****************************************************************************
  Function:
    void ModBus_Manage

  Summary:
    Presenta las tramas MIWI de Modbus para la capa de APP MIWI.

  Description:
	Presenta las tramas MIWI de Modbus para la capa de APP MIWI.

  Precondition:
    Requiere biblioteca Modbus.h

  Parameters:
    None

  Returns:
    None
 ***************************************************************************/
void ModBus_Manage(void){
    /*verifico la llegada de nuevos paquetes modbus de forma asincronica y de estados.
     otra forma es mediante interrupcion de llegada de paquetes, pero puede interrumpir
     una tarea de mayor interes*/
    while(PIR1bits.RC1IF) //PIR1bits_t.RC1IF) Se mantiene en alto por mientras haya fifo lleno en usart
        /* Conformo los datos en un estructura:
           uint8_t address; MODBUS-ADD
           uint8_t len;                                //number of bytes in the message received
           function func;                              //the function of the message received
           exception error;                            //error recieved, if any
           uint8_t data[MODBUS_SERIAL_RX_BUFFER_SIZE]; //data of the message received
           modbus_rx.data[]                            //arreglo de datos !!!!
         */
        incomming_modbus_serial();
        
    
    return;
}
