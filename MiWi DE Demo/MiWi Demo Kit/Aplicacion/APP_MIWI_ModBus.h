/* Microchip Technology Inc. and its subsidiaries.  You may use this software 
 * and any derivatives exclusively with Microchip products. 
 * 
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER 
 * EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED 
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A 
 * PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION 
 * WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
 * INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
 * WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS 
 * BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE 
 * FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS 
 * IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF 
 * ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE 
 * TERMS. 
 */

/* 
 * File:   
 * Author: CBA 
 * Comments:
 *   Procedimientos que atienten los paquetes Modbus y los enmarcan en el 
 *  formato de MIWI.
 *  Se basa en la biblioteca Modbus.h generica para todas la aplicaciones restantes
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef XC_HEADER_TEMPLATE_H
#define	XC_HEADER_TEMPLATE_H

// TODO Insert appropriate #include <>
#include <xc.h> // include processor files - each processor file is guarded.  
#include <stdint.h>

#define Length_Trama 127  //payload basica de MIWI 802.15.4
#define Length_miwi_header 4  //longitud de header miwi
#define Length_miwi_chk 4  //longitud de crc
#define Length_Add 4      //longitud de direcciones
#define Length_buffer_RX 1//2
#define Length_buffer_TX 1//2




typedef struct Message_t_def
    {
        uint8_t Adress[Length_Add]; //direccion de sclave MB
        uint8_t Payload[Length_Trama];        
    } Message_t;

typedef struct Buffer_MODMIWI_t_def
    {
        Message_t Message[Length_buffer_RX]; //paquete terminados por ModBus_M APP recibidos
        uint8_t point; //indica donde esta el buffer
    } Buffer_MODMIWI_t_;
    

extern Buffer_MODMIWI_t_ Rx_Buffer_MODMIWI; //paquete terminados por ModBusAPP recibidos

extern Buffer_MODMIWI_t_ Tx_Buffer_MODMIWI; //paquete terminados por ModBusAPP para transmitir

// Comment a function and leverage automatic documentation with slash star star
/**
    <p><b>void ModBus_Manage(void):</b></p>
  
    <p><b>Summary:</b></p>

    <p><b>Description:</b></p>

    <p><b>Precondition:</b></p>

    <p><b>Parameters:</b></p>

    <p><b>Returns:</b></p>

    <p><b>Example:</b></p>
    <code>
 
    </code>

    <p><b>Remarks:</b></p>
 */
// TODO Insert declarations or function prototypes (right here) to leverage 
// live documentation
bool ModBus_UnicastAddress(uint8_t);
bool ModBus_MessageAvailable(void);
bool Modbus_DiscardMessage(void);
void ModBus_Manage(void);

#endif	/* XC_HEADER_TEMPLATE_H */

