/* 
 * File:   MIWI.h
 * Author: cba
 *
 * Created on April 9, 2018, 9:52 AM
 */

#ifndef MIWI_H
#define	MIWI_H

#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif

#endif	/* MIWI_H */

typedef enum  State_t_Def{
    RECB=0,
    TRANS_W
}State_t; // estado inicial al inicio


void Ini_MIWI(void);
bool FunRx(void);
bool FunTx(void);
//bool Serv_MIWI(void);
bool Serv_MIWI(State_t);
