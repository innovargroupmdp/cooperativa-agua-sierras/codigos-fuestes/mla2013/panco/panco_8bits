/*
 * File:   APL_Fun.c
 * Author: cba
 *
 * Created on 23 de mayo de 2018, 11:39
 */


#include <stddef.h>
#include <string.h>

#include "APL_Fun.h"
#include "MIWI.h"
#include "App.h"



void Clear_Apps(void){
    //APP
    App.ID=0;
    App.Tipo=0;; // T=1: Normal ||T=2: Asociacion || T=3: Time ||T=4: Normal
    App.Secuencia=0;
    App.Nro_intentos=0;
    App.Noise=0;
    App.Signal=0;
    App.Bat=0;
    App.Slot=0;
    App.DateTx.Day=0;
    App.DateTx.Hou=0;
    App.DateTx.Min=0;
    App.DateTx.Mon=0;
    App.DateTx.Seg=0;
    App.DateTx.Yea=0;
    App.MedidaRE.Mesure=0;
    App.MedidaRE.DateMesu.Day=0;
    App.MedidaRE.DateMesu.Mon=0;
    App.MedidaRE.DateMesu.Hou=0;
    App.MedidaDif[0]=0;
   
    App.SlotDif=0;
    
    //APP1
    App1.ID=0;
    App1.Tipo=0;; // T=1: Normal ||T=2: Asociacion || T=3: Time ||T=4: Normal
    App1.Secuencia=0;
    App1.Nro_intentos=0;
    App1.Noise=0;
    App1.Signal=0;
    App1.Bat=0;
    App1.Slot=0;
    App1.DateTx.Day=0;
    App1.DateTx.Hou=0;
    App1.DateTx.Min=0;
    App1.DateTx.Mon=0;
    App1.DateTx.Seg=0;
    App1.DateTx.Yea=0;
    App1.MedidaRE.Mesure=0;
    App1.MedidaRE.DateMesu.Day=0;
    App1.MedidaRE.DateMesu.Mon=0;
    App1.MedidaRE.DateMesu.Hou=0;
    App1.MedidaDif[0]=0;
       
    App1.SlotDif=0;
    
};

void Swap_App1_to_App(void){
    
    uint8_t *p=NULL,*p1=NULL,i;
    static uint8_t j=0;
    
    p=&App1.Tipo;
    p1=&App.Tipo;
     
    App.ID=App1.ID;

    for(i=0;i<6;i++){                    
        *p1=*p;
        p++;
        p1++;
    }

    BufferRX.Buffer[j].Slot=App1.Slot;

    p=&App1.DateTx.Seg; 
    p1=&BufferRX.Buffer[j].DateTx.Seg; //GW_RTCC.Seg;

    for(i=0;i<6;i++){ 
        *p1=*p;
        p++;
        p1++;
    }

    p=&App1.MedidaRE.DateMesu.Hou;
    p1 =&(BufferRX.Buffer[j].MedidaRE.DateMesu.Hou);


        for (i = 0; i < 3; i++) {
            *p1= *p ;
            p++;
            p1++;
        }
    
    for (i = 0; i < Tomas; i++) {

        BufferRX.Buffer[j].MedidaDif[i]=App1.MedidaDif[i];
        
    }
    
}

void ReadRTCC(void){
    return;
}
void RTCC_Set(uint8_t* p){
    return;
}

void RTCC_Get(void){
    uint8_t *p1,*p,i;
    ReadRTCC();
    p1=& GW_RTCC.Seg; 
    p=&App1.DateTx.Seg;

    for(i=0;i<6;i++){ 
        *p=*p1;
        p++;
        p1++;
    }
    
    return;
}

void Search_Control(void){
               
    uint8_t i=0;        
   
    // Uso indice ID=0 indica elemento vector sin uso.
    while(BaseSlot[i][0]!=App1.ID && i<(BaseSlot_LEN-1) && BaseSlot[i][0]!=0 ) 
        i++;
    
    if (BaseSlot[i][0]!=App1.ID) { // descarto el elemento actual si llega al final
        App1.Slot=0;// no habra cambios
        return;    
    }
        
    App1.Slot=BaseSlot[i][1]; // elemento actual filtrado

    // limpio la base y ordeno
        
    while(i<49){ //|| BaseSlot[i][1]!=0) { // caso de ser cualquiera

        BaseSlot[i][0]=BaseSlot[i+1][0];
        BaseSlot[i][1]=BaseSlot[i+1][1];        
        i++;
    }
    BaseSlot[i][0]=0;          // caso de ser el ultimo elemento 49
    BaseSlot[i][1]=0;
    
    
    return;
    
} 
void Put_Base(void){
    
    uint8_t i=0;        
    
    while(BaseSlot[i][0]!=0 && i<(BaseSlot_LEN-1))
        i++;
        
    if (BaseSlot[i][0]==0) {
        
        BaseSlot[i][0]=App.ID;
        BaseSlot[i][1]=App.Slot;
        
    }
    
} 

//--------------- buffer de tramas normales-------------------------

uint8_t Put_BufferRx(void) {
    
    uint8_t *p=NULL,*p1=NULL,i,j;
    static bool s=1;
    
    if (s){ //SET FIRST TIME POINT
        BufferRX.point=0;
        s--;
    }
    
    j=BufferRX.point;
    
    BufferRX.Buffer[j].ID=App1.ID;
    
    p=&App1.Tipo;
    p1=&BufferRX.Buffer[j].Tipo;

    for(i=0;i<6;i++){                    
        *p1=*p;
        p++;
        p1++;
    }

    BufferRX.Buffer[j].Slot=App1.Slot;
    
    BufferRX.Buffer[j].DateTx.Seg=App1.DateTx.Seg;
    BufferRX.Buffer[j].DateTx.Min=App1.DateTx.Min;
    BufferRX.Buffer[j].DateTx.Hou=App1.DateTx.Hou;
    
    //p=&App1.DateTx.Seg; 
    //p1=&BufferRX[j].DateTx.Seg; //GW_RTCC.Seg;
    //solo seg min hour
    //for(i=0;i<3;i++){ 
    //    *p1=*p;
    //    p++;
    //    p1++;
   // }
    
    BufferRX.Buffer[j].MedidaRE.Mesure=App1.MedidaRE.Mesure; //ultima medida
    BufferRX.Buffer[j].MedidaRE.DateMesu.Hou=App1.MedidaRE.DateMesu.Hou; //ultima medida
    BufferRX.Buffer[j].MedidaRE.DateMesu.Day=App1.MedidaRE.DateMesu.Day; //ultima medida
    BufferRX.Buffer[j].MedidaRE.DateMesu.Mon=App1.MedidaRE.DateMesu.Mon; //ultima medida
    
    p=App1.MedidaDif;
    p1=BufferRX.Buffer[j].MedidaDif;
    
    for (i = 0; i < Tomas; i++) {
        *p1= *p;
        p++;
        p1++;
    }
    
    return(BufferRX.point++);// incremento el buffer.
   
}  

void Clear_BufferRx(void){
    
    uint8_t *p=NULL,*p1=NULL,i,d;
    static uint8_t j=0; 
    
    d=BufferRX.send_point+1;
    
    while (BufferRX.Buffer[j+d].Tipo!=0 && j<=BufferRX.send_point && (j+d)<BufferRX_length){
        
        BufferRX.Buffer[j].ID=BufferRX.Buffer[j+d].ID;
        
        //BufferRX.Buffer[j].Tipo=BufferRX.Buffer[j+BufferRX.send_point+1].Tipo;
        
        p=&BufferRX.Buffer[j].Tipo;
        p1=&BufferRX.Buffer[j+d].Tipo;
        
        for(i=0;i<6;i++){  // de tipo -> bat                  
            *p=*p1;
            p++;
            p1++;
        }
        
        BufferRX.Buffer[j].DateTx.Seg=BufferRX.Buffer[j+d].DateTx.Seg;
        BufferRX.Buffer[j].DateTx.Min=BufferRX.Buffer[j+d].DateTx.Min;
        BufferRX.Buffer[j].DateTx.Hou=BufferRX.Buffer[j+d].DateTx.Hou;
    
        BufferRX.Buffer[j].MedidaRE.DateMesu.Hou=BufferRX.Buffer[j+d].MedidaRE.DateMesu.Hou; //ultima medida
        BufferRX.Buffer[j].MedidaRE.DateMesu.Day=BufferRX.Buffer[j+d].MedidaRE.DateMesu.Day; //ultima medida
        BufferRX.Buffer[j].MedidaRE.DateMesu.Mon=BufferRX.Buffer[j+d].MedidaRE.DateMesu.Mon; //ultima medida
        
        BufferRX.Buffer[j].MedidaRE.Mesure=BufferRX.Buffer[j+d].MedidaRE.Mesure; //ultima medida
        
        BufferRX.Buffer[j].Slot=BufferRX.Buffer[j+d].Slot;
                          
        //p1=&BufferRX[j+10].DateTx.Seg; 
        //p=&BufferRX[j].DateTx.Seg; //GW_RTCC.Seg;

        p=App1.MedidaDif;
        p1=BufferRX.Buffer[j].MedidaDif;
        
        /*------------todas las medidas---------------------------------------*/
        for (i = 0; i < Tomas; i++) { 
                *p= *p1 ;
                p++;
                p1++;            
        }
        
        BufferRX.Buffer[j+d].ID=0; //BLANQUEO  DE SEGURIDAD!!! 
        BufferRX.Buffer[j+d].Tipo=0;
        
        j++;// incremento el buffer.
    }
    
    BufferRX.point=j;//apunto al proximo elemento libre en el BuffeRX  
       
    Buff_Rx_MiWi=false;
}

bool Resp_MIWI_NORM(void){ // case 1
    /*LOS DATOS DE RTCC Y DE SLOT SON AGREGADOS AQUI PARA TRANSMITIR POR MIWI*/
    static bool s=true;
    
    if (s){
        s--;
        RTCC_Get(); //Ubica todos los datos de tiempo en App1.DateTx para ser transmitidos a MiWi
        Search_Control();//busca cambios en el Slot de comunicacion y los guarda en App1.slot, acomoda BaseSlot.
    }    
    if (Serv_MIWI(TRANS_W)){           //Responde a la subida  toma datos a app1
        s++;
        return(true);
    }
    return(false);
}

bool Resp_MIWI_ASOC(void){
    static uint8_t j=0;
    static bool s=true;
                
    if(s){
        while(j<Bufferlength){ 
            if (Buffer[j].Sent && Buffer[j].Rece && (Buffer[j].ID) ){ //consulta a la buffer

                s--;
                App1.ID=Buffer[j].Element.ID; 
                App1.Slot=Buffer[j].Element.Slot;
                App1.SlotDif=Buffer[j].ID;//id que envia el servidor que antes resultaba 0
                //Buffer[j].Rece=true;
                //AddMIWI=Buffer[j].Add;
                //rireccopm de respuesta
                //memcpy(AddMIWI,Buffer[j].Add,ADDRESS_LEN);// machea ID-ADD incial con la ID del medidor brindada
                RTCC_Get(); // fecha en App1.DAte
                //Buffer[j].Add=AddMIWI; //sale de aplicacion MIWI no esta en app1

                Serv_MIWI(TRANS_W);          //Responde a la asociacion  toma los datos a app1
                return(false); 
            }

            j++; 

        }
        if(j==(Bufferlength)) j=0;

    }else if (Serv_MIWI(TRANS_W)){

        s++;
        //--------blanqueo-----------------------------
        Buffer[j].Element.ID=0;
        Buffer[j].Element.Tipo=0;
        Buffer[j].Element.Slot=0;
        Buffer[j].Element.Secuencia=0;// secuencia de medidor
        Buffer[j].Sec_GW=0; 
        Buffer[j].Sent=false; 
        Buffer[j].Rece=false;
        Buffer[j].ID=0;
    //  Buffer[j].Add={0,0,0,0,0,0,0,0};
    //  Buffer[j].Add={{0}}; //sale de aplicacion MIWI no esta en app1
        return(true);    
    }                
    
    return(false);
}

bool Serv_Buff_stat(void){
    return(false);
}

bool Put_Buffer_TcpIp(void){ // lo coloca APP TCP PARa que MIWI opere
    uint8_t j=0;  
    while(j<Bufferlength){ 
        
        if (Buffer[j].Sent && (Buffer[j].Sec_GW==App.Secuencia) ){

            Buffer[j].ID=App.ID; //id que llega del servidor, el elemen.ID es la MAC queda inalterado en el RFD pero autenticado en el servidor
            Buffer[j].Element.Slot=App.Slot; //slot asociado
            Buffer[j].Rece=true;
            return(true);
        }       
        j++;        
    }
    return(false);
}

bool Get_BufferRx_TcpIp(void){ // Lo usa MIWI para operar sobre el. Lo coloca TCP
    
     return (true);
}
bool Put_Buffer_MIWI(void){ //lo coloca MIWI para que luego lo tome TCP
    uint8_t i=0;
    static uint8_t j=0;
    //verifico si es que ya hay un pedido existente
    while  (i<Bufferlength){
        if  (App1.ID==Buffer[i].Element.ID && Buffer[i].Rece==false){ //MAC en buffer igual al de App1 trama existente del  mismo medidor pero anterior
            
            Buffer[i].Element.Tipo=2;
            Buffer[i].Element.Slot=0;
            Buffer[i].Element.Secuencia=App1.Secuencia;// secuencia de medidor
            Buffer[i].Sec_GW=0; 
            Buffer[i].Sent=false; 
            Buffer[i].Rece=false;
            Buffer[i].ID=0;
           // Buffer[i].Add=AddMIWI; //sale de aplicacion MIWI no esta en app1
           // memcpy(Buffer[i].Add,AddMIWI,ADDRESS_LEN); //solo para PANCO en el gateway no
            return(true);
        }
        
        i++;
    }
    i=0;
    // De existir uno presente y ya contestado por servior no enviado miwi (caso fuera de estado)
    while  (i<Bufferlength){
        if  (App1.ID==Buffer[i].Element.ID && Buffer[i].Rece){ //MAC en buffer igual al de App1 trama existente del  mismo medidor pero anterior
            
            //Actualizo la secuencia para ser respondida con la ID servidor
            Buffer[i].Element.Secuencia=App1.Secuencia;// secuencia de medidor
           
            return(true);
        }        
        i++;
    }
    i=0;
    // De NO existir un pedido lo incorporo para ese IDmac
    while  (i<Bufferlength){
        
        if  (!Buffer[i].Element.ID){ //si es igual a cero
            
            Buffer[i].Element.ID=App1.ID;
            Buffer[i].Element.Tipo=2;
            Buffer[i].Element.Slot=0;
            Buffer[i].Element.Secuencia=App1.Secuencia;// secuencia de medidor
            Buffer[i].Sec_GW=0; 
            Buffer[i].Sent=false; 
            Buffer[i].Rece=false;
            Buffer[i].ID=0;
            //Buffer[i].Add=AddMIWI; //sale de aplicacion MIWI no esta en app1
            //memcpy(Buffer[i].Add,AddMIWI,ADDRESS_LEN);
            return(true);
        }
        
        i++;
    }
    
    // roll -buffer lleno
    i=0;
    while  (i<(Bufferlength-3)){ //si fue enviado pero no recibido margen 3 ms nuevos
        if  (!Buffer[i].Rece && Buffer[i].Sent ){ //si es igual a cero

            Buffer[i].Element.ID=App1.ID;
            Buffer[i].Element.Tipo=2;
            Buffer[i].Element.Slot=0;
            Buffer[i].Element.Secuencia=App1.Secuencia;// secuencia de medidor
            Buffer[i].Sec_GW=0; 
            Buffer[i].Sent=false; 
            Buffer[i].Rece=false;
            Buffer[i].ID=0;
            //Buffer[i].Add=AddMIWI; //sale de aplicacion MIWI no esta en app1
            //memcpy(Buffer[i].Add,AddMIWI,ADDRESS_LEN);
            return(true);
        }
        i++;        
    }
    // caso defauld
    Buffer[j].Element.ID=App1.ID;
    Buffer[j].Element.Tipo=2;
    Buffer[j].Element.Slot=0;
    Buffer[j].Element.Secuencia=App1.Secuencia;// secuencia de medidor
    Buffer[j].Sec_GW=0; 
    Buffer[j].Sent=false; 
    Buffer[j].Rece=false;
    Buffer[i].ID=0;
    //memcpy(Buffer[j].Add,AddMIWI,ADDRESS_LEN);
    //Buffer[j].Add=AddMIWI; //sale de aplicacion MIWI no esta en app1

    
    if (j<(Bufferlength-1))j++;
        else   j=0;
    
    return(false);
}
/* bool Get_Buffer_MIWI(void)
 *  para casos de control no se usa en el primer proyecto
 * 
 *  */
/*
bool Get_Buffer_MIWI(void){// lo usa TCP para operar y colocar en su TCP TX
    
    uint8_t i,q=0;
    static uint8_t j=0;
    uint8_t *p=NULL,*p1=NULL;
    
    
    while  (q<Bufferlength){
    
        if  (!(*p1)){            //Sent=0
            
            App.ID=Buffer[q].Element.ID;
            p=&App.Tipo;
            p1=&Buffer[0].Element.Tipo;

            for(i=0;i<6;i++){                    
                *p=*p1;
                p++;
                p1++;
            }

            BufferRX.Buffer[j].Slot=App1.Slot;
            
            p=&App1.DateTx.Seg; 
            p1=&BufferRX.Buffer[j].DateTx.Seg; //GW_RTCC.Seg;

            for(i=0;i<6;i++){ 
                *p=*p1;
                p++;
                p1++;
            }
            
            p=&App1.MedidaRE.DateMesu.Hou;
            p1 =&BufferRX.Buffer[j].MedidaRE.DateMesu.Hou;
            
            for (i = 0; i <3 ; i++) {
                    *p= *p1 ;
                    p++;
                    p1++;
            }        

            for (i = 0; i < Tomas; i++) {

                App1.MedidaDif[i]=BufferRX.Buffer[j].MedidaDif[i];

            }
            
            
            return(true);
        }
       p1++;
    }   
    return(false);
} 

*/    

bool Get_Tipo2_BufferRx_MIWI(void){
    uint8_t i=0;
    
    while(i<Bufferlength){ // por que razon??? verificar!!!
        
        if (Buffer[i].Element.Tipo==2 && Buffer[i].ID && Buffer[i].Rece ){ //si no es igual a cero

            return(true);
        }
        i++;        
    }
    return(false);
}

bool Get_Buffer_TcpIp(void){
    
    uint8_t i=0;
    while  (i<Bufferlength){
    
        if  (!Buffer[i].Sent && (Buffer[i].Element.ID!=0) ){ //Sent=0
            
            App.ID=Buffer[i].Element.ID;      // MAC
            App.Tipo=2;
            Buffer[i].Sec_GW=GW_Secuencia; 
            Buffer[i].Sent=true; 
            return(true);
        }
        i++;
    }   
    return(false);
}
   