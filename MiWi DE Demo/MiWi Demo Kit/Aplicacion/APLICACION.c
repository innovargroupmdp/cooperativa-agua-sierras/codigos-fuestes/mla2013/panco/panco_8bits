
//#include <stddef.h>

//#include <string.h>


#include <stddef.h>
#include <string.h>

#include "App.h"
#include "APL_Fun.h"
#include "MIWI.h"
#include "APLICACION.h"
#include "GenericTypeDefs.h"



   
void Serv_APP(void){
    
    BYTE *p=NULL,*p1=NULL,i;
   
    static BYTE st=0,st1=0;
    
    static BOOL N=FALSE;

    //---------------------------------------------
    //-----------------MODBUS---------------------
    //---------------------------------------------    
    // Ingreso cuando el buffer esta lleno, cuando hay tramas
    // de time y asociacion estas ultimas tienen prioridad
    //( Buff_Rx_MiWi |(App1.Tipo==2)||(App1.Tipo==3) )| & 
    
    if ( !Sem_TCP_DONE ){   
            
    //tiene que haber un semaforo que indique el fin DONE de TCP para que no evolucione o ingrese aqui
    // Debe ser llamada luego que se identifica que tipo de Paquete enviar
    // Identico si hay necesidad de transmitir, estados permanentes internos para seguir
    /*leo buffer de recepcion miwi e untercambio las variables a App*/
        
    //------------------------- STACK    
        if (st==0){ //tiene que haber datos
            if (Get_Buffer_TcpIp())   //solo entra una vez obtego App.Tipo  // PRIORIDAD 1: TIP.2
                st=1;                 //correspondiente
            else if(Buff_Rx_MiWi){    //para sobre escribir                 // PRIORIDAD 2: TIP.1
                App.Tipo=1;           //CUANDO ENTRE AL APP SERVIDOR ENVIA BUFFER TRAMAS NORMARLES
                st=1;
            }
        }    
        else if(st==1) {
            
            if(DONE_st==TCP_Client()){ // seteado App.Tipo puedo enviar consula al servidor luego en DONE la RESPUESTA ESTA AMACENADA EN APP1
                Sem_TCP_DONE=true;
                st=0;
            }
        }
    }
    else 
        if(Sem_TCP_DONE && App.Tipo ){ //Sem_TCP_DONE 1: libre(servicio anterior efectuado listo para transmitir u 
            //operar sobre los datos recibidos) 0:ocupado(endesarrollo)
            /*Entrar aqui indica que se todos los datos del servicio TCPIP son validos
             * Seguido de procesa los datos recibidos y se efectuan las operaciones correspondientes
             * segun sea necesario para comunicarlas a los servicios Miwi o para operar sorbe el GateWay
            */
            /*las variables a App pasan el buffer de TX miwi */
            //------------------------- APP--------------------------
            //------------------------- APP--------------------------
            //------------------------- APP--------------------------
            switch (App.Tipo){  //informacion desde TCP-IP a MIWI

                case 1: //-----------NORMAL 
                    /*se enviaron recibieron los datos envidos de los medidores al servidor remoto*/
                    //incremento la secuencia nada mas
                    // Envi� el buffer(n) y ahora limpio el (n+1)
                    //limpio el bufferRX, solo aquellos enviados el resto se hubica en cola.
                    Clear_BufferRx();
                break;
                case 2: //----------- ASOCIACION
                    /*Se asocio el medidor al servidor. llegaron los datos para enviar al medidor*/
                    //Recibo ID que envia el servidor
                    Put_Buffer_TcpIp();
                break;
                case 3: //-----------TIME
                    /*El servidor respondio ante la solicitud de ajuste de RTCC local llegaron los datos para el GW*/

                    //-------------- Paso de App.DateTx => GW_RTCC;
                    p=&App.DateTx.Seg; 
                    RTCC_Set(p);      // actualiza el RTCC
                    p1=&GW_RTCC.Seg; //GW_RTCC.Seg;

                    for(i=0;i<6;i++){ 
                        *p1=*p;
                        p++;
                        p1++;
                    }
                break;
                case 4: //-----------Asociacion GW
                    /*Se solicito una asociacion de GW llegaron los datos para el GW*/ 
                    Id_GW=App.ID;
                    strcpy(Pass,Pass_AU);
                break;            
                case 5: //-----------CONTROL
                    /*El servidor solicito la solicitud de ajuste de slots medidor*/ 
                    Put_Base();  
                break;    
            }
            
        App.Tipo=0;
        Sem_Free_App=true;// libero el recurso del registro 
        GW_Secuencia++;
        Sem_TCP_DONE=false;// puedo entrar al servicio de escucha tcp
    }
    //------------------------------------------------------------------------
    //---------------------MI-WI----------------------------------------------
    //------------------------------------------------------------------------
    
    if(!Sem_MIWI_DONE){   //ingresa siempre aqui, verifico estados de la red
    //------------------------- STACK -------------------------------------   
        Serv_MIWI(RECB); //transmito para solicitar si hay tramas nuevas en el PANCO mediante MB devuelvo 
                          //sobre App1.#
                          //Set Sem_MIWI_DONE
        //------------Consideraciones para ingreso en Caso 2---------------------
        if(st1 &&(App1.Tipo==0) && !Sem_MIWI_DONE){ // Verifico que el servicio no este ocupado
            App1.Tipo=2;                           // App1 en blanco y el ST1 indicando trama Asoc pendiente
            Sem_MIWI_DONE=true;// tomo el servicio entro en el siguiente estado en caso 2
            
        }else if(Sem_MIWI_DONE && App1.Tipo==2){ // estado de trama nueva App1 2
            N=1;
            }
                    
    }
    else 
        if(Sem_MIWI_DONE && App1.Tipo){ //Sem_MIWI_DONE 1 libre (para recibir o 
                                        //transmitir O OPERAR SOBRE LOS DATOS) 
                                        //, 0 ocupado (en proceso PREVIO))
        //------------------------- APP---------------------------------------
        //------------------------- APP---------------------------------------
        //------------------------- APP---------------------------------------
            switch (App1.Tipo){  //informacion desde MIWI a TCP-IP       
                case 1: //-----------NORMAL
                    //-----------COLOCO LAS TRAMAS RECIBIDAS EN EL BUFFER    
                    if (Put_BufferRx()>AlarmaBuffer)
                        Buff_Rx_MiWi=true;    //alarma que indica a tcp-ip que debe transmitir este buffer 
                    //-----------REQUIERO RESPONDER AL MEDIDOR LA LLEGADA DE LOS DATOS CON EL 
                    ///SETEO DEL RTCC DEL MEDIDOR 
                    //-----------Y DE SLOT DE SER REQUERIDO VERIFICO EN UNA LISTA DE ID-SLOT
                    if (Resp_MIWI_NORM()){
                        App1.Tipo=0;
                        Sem_Free_App1=true;// libero el recurso del registro 
                        Sem_MIWI_DONE=false; //TOMO EL RECURSO, ASEGURO UN NUEVO PROCESO
                    }
                    // reserve el recurso ahora debo transmitir por miwi seleccionando el estado segun App1.tipo

                break;
                case 2: //---------- ASOCIACION
                // elevo la trama directamente a TCP por ser asociacion.
                // Todo App1.# tiene la informacion para ello
                // Esto va en el buffer    
                    if (N){
                        N=0;
                        Put_Buffer_MIWI(); // coloco trama nueva en buffer, luego APPTCP Verifica
                        //no modifico Sem_MIWI_DONE ni tipo la proxima iteracion entro directo a este caso
                    } 
                    if (!st1) 
                        st1=1; // estado que indica que entro una trama asociacion, En APPTCP Verifico

                    else{ // existe una trama previa

                        if(Resp_MIWI_ASOC()){// servicio de respuesta si hay trama de respuesta
                            App1.Tipo=0;
                            Sem_Free_App1=true;// libero el recurso del registro 
                            Sem_MIWI_DONE=false; //libero EL RECURSO, ASEGURO UN NUEVO PROCESO
                        } 

                        if(!Get_Tipo2_BufferRx_MIWI()) // Verifico si hay mas tramas en cola
                            st1=0; // NO  hay mas tramas en buffer
                    }

                break;
               
                case 3: //-----------TIME
                break;
                
                case 4: //-----------CONTROL
                break;    
                
            }
            //App1.Tipo=0;
            //Sem_Free_App1=true;// libero el recurso del registro 
            //Sem_MIWI_DONE=false; //TOMO EL RECURSO, ASEGURO UN NUEVO PROCESO
    }
}
